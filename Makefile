# Compiler
CC = g++

# Project name
PROJECT = AnalyzeNtuple

# Directories
OBJDIR = obj
SRCDIR = src

# Libraries
ROOTCFLAGS = $(shell root-config --cflags) -O3 -std=c++11 
ROOTLIBS   = $(shell root-config --libs) -lTreePlayer  -Wall -fPIC -lProof -lProofPlayer 
ROOTGLIBS    	:= $(shell root-config --glibs)
SOFLAGS		= -O -shared 

# Files and folders
SRCS    = $(shell find $(SRCDIR) -type f -name '*.cpp')
HEADERS = $(shell find $(SRCDIR) -type f \( -iname "*.h" ! -iname "*Linkdef*" \))
SRCDIRS = $(shell find $(SRCDIR) -type d | sed 's/$(SRCDIR)/./g' )
OBJS    = $(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SRCS))
SOFLAGS 	+= $(ROOTGLIBS) -L./

# Targets

all: $(PROJECT) clean

$(PROJECT): buildrepo MyDict.cpp $(OBJS)
	$(CC) -o $@ MyDict.cpp $(OBJS) $(ROOTCFLAGS) $(ROOTLIBS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) -o $@ $< -c $(ROOTCFLAGS)

MyDict.cpp: $(HEADERS) src/Linkdef.h
	rootcint -f $@ -c $(ROOTCFLAGS) -p $^

lib%.so: %.o
	$(CXX) $(SOFLAGS) -o $@ $^

clean:
	rm -rf $(OBJDIR)
	rm MyDict.cpp
	rm MyDict_rdict.pcm

buildrepo:
	@$(call make-repo)

# Create obj directory structure
define make-repo
	mkdir -p $(OBJDIR)
	for dir in $(SRCDIRS); \
	do \
		mkdir -p $(OBJDIR)/$$dir; \
	done
endef
