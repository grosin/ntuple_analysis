#include <TNtuple.h>
#include <vector>
#include <TH1F.h>
#include <string>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <iostream>
#include  <TH1D.h>
#include <TH2D.h>
class loop_ntuple{
 public:
  loop_ntuple(TTree *tnt,std::string name="met_bdt",int nbins=20,double min=0,double max=0.1);
  std::string name;
  //define variables
  float weight;
  double bdt_met;
  double TrackMET;
  float MET;
  float mjj;
  float mll;
  float r_mll;
  float r_mjj;
  float r_weight;
  std::vector<float> *truth_jet_phi;
  std::vector<float> *truth_jet_eta;
  std::vector<float> *truth_jet_pt;

  std::vector<float> *reco_jet_phi;
  std::vector<float> *reco_jet_eta;
  std::vector<float> *reco_jet_pt;


  std::vector<float> *truth_lep_phi;
  std::vector<float> *truth_lep_eta;
  std::vector<float> *truth_lep_pt;

  std::vector<float> *reco_lep_phi;
  std::vector<float> *reco_lep_eta;
  std::vector<float> *reco_lep_pt;
  int passReco;
  int fiducial;
  int preSelection;
  int passedCJV;
  int passedOLV;
  
  //define histograms

  //met hists
  TH1F * bdt_Zjets_hist;
  TH1F* TrackMETRel_withJets;
  TH1F* TrackMETRel_noJets;
  TH1F * METRel_withJets;
  TH1F * METRel_noJets;
  TH1F * TrackMET_hist;
  TH1F * MET_hist;
  TH1F * MET_CST;
  TH1F * MET_TST;
  TH1F* Truth_MET;
  
  //vertex
  TH1F * lead_jet_pt_truth;
  TH1F *sub_jet_pt_truth;
  TH1F * lead_jet_phi_truth;
  TH1F *sub_jet_phi_truth;
  TH1F * lead_jet_eta_truth;
  TH1F *sub_jet_eta_truth;
  
  TH1F * lead_jet_pt_reco;
  TH1F *sub_jet_pt_reco;
  TH1F * lead_jet_phi_reco;
  TH1F *sub_jet_phi_reco;
  TH1F * lead_jet_eta_reco;
  TH1F *sub_jet_eta_reco;

  //scalar
  TH1F * jet_dphi_truth;
  TH1F *jet_dr_truth;
  TH1F *jet_dEta_truth;

  TH1F * lep_dphi_truth;
  TH1F *lep_dr_truth;
  TH1F *lep_dEta_truth;
  
  TH1F * jet_dphi_reco;
  TH1F *jet_dr_reco;
  TH1F *jet_dEta_reco;

  TH1F * mjj_truth;
  TH1F *mjj_reco;
  TH1F * mll_truth;
  TH1F *mll_reco;

  //VIP hists
  TH1D * detajj_true;
  TH1D * detall_true;
  TH1D * detajj_reco;
  TH1D * detall_reco;

  TH1D * detajj_recoSel_rw_true;
  TH2D * detajj_migration;
  TH1D * detajj_trueAreco_rw_reco;
  TH1D * detajj_trueAreco_rw_true;
  TH1D * detajj_trueAreco_tw_true;

  TH1D * detall_recoSel_rw_true;
  TH2D * detall_migration;
  TH1D * detall_trueAreco_rw_reco;
  TH1D * detall_trueAreco_rw_true;
  TH1D * detall_trueAreco_tw_true;

  TH1D * mjj_recoSel_rw_true;
  TH2D * mjj_migration;
  TH1D * mjj_trueAreco_rw_reco;
  TH1D * mjj_trueAreco_rw_true;
  TH1D * mjj_trueAreco_tw_true;

  TH1D * mll_recoSel_rw_true;
  TH2D * mll_migration;
  TH1D * mll_trueAreco_rw_reco;
  TH1D * mll_trueAreco_rw_true;
  TH1D * mll_trueAreco_tw_true;
  
  //class methods
  void fill_hists(double cut);
  double effeciency(double cut);
  void fill_met_hists();
  void vip_hists();
  
 private:
  TTree * my_tree;
};
template<typename T>
void DrawHist(T *hist, std::string type="pdf"){
  TCanvas *cDraw= new TCanvas("cDraw", "cDraw",900,900);
  cDraw->cd();
  std::string name=hist->GetName();
  name=name+"."+type;
  hist->SetFillStyle(0);
  hist->SetMarkerSize(0.8);
  hist->SetMarkerStyle(1);
  hist->Draw("HIST");
  cDraw->Print(name.c_str());
  delete cDraw;
}

template<typename T>
double DrawHistograms(std::vector<T*> &hists, std::string type="pdf",std::string filname="histograms"){
  TCanvas *cDraw= new TCanvas("cDraw", "cDraw",900,900);
  TPad *pad1 = new TPad("pad1", "pad1",0.,.3,1.,1);
  TPad *pad2 = new TPad("pad2", "pad2",0.,0.,1.,0.3);

  auto legend = new TLegend(0.1,0.8,0.48,0.9);
  //cDraw->cd();
  bool first=true;
  int i=2;
  int j=2;
  std::cout<<"looping over  hists\n";
  pad1->SetBottomMargin(0);
  pad1->Draw();
  pad1->cd();
  TH1F *h3 ;
  double  stats[2][4];
  
  for(auto &h :hists){
    std::cout<<"setting fill style\n";
    //h->SetMarkerColor(i);
    //h->SetLineColor(i++);
    //h->SetMarkerSize(0.8);
    //h->SetMarkerStyle(j++);
    std::string name=h->GetName();
    std::cout<<"drawing: "<<name<<"\n";
    legend->AddEntry(h,name.c_str(),"f");
    if(first){
      first=false;
      h3 = (TH1F*)h->Clone("h3");
      h->SetFillStyle(1001);
      h->SetFillColor(i++);
      h->Draw("HIST");
      h->GetStats(stats[0]);
    }
    else{
      h->SetFillStyle(1001);
      h->SetFillColor(i++);
      h3->Sumw2();
      h3->Divide(h,h3,1,1,"B");
      //h3->Divide(h);
      std::cout<<"drawing : "<<name<<"\n";
      h->Draw("HIST SAME");
      h->GetStats(stats[1]);
    }
  }
  h3->GetYaxis()->SetRangeUser(0.8,1.2);
  legend->Draw();
  std::string filename=filname+"."+type;
  cDraw->cd();
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.2);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();
  h3->Draw();
  cDraw->Print(filename.c_str());
  delete cDraw;
  std::cout<<"stats for full thing"<<stats[0][0]<<"\n";
    std::cout<<"stats for full thing"<<stats[0][2]<<"\n";

  std::cout<<"stats for cut "<<stats[1][0]<<"\n";
  std::cout<<"stats for cut  "<<stats[1][2]<<"\n";
  
  return stats[1][0]/stats[0][0];
}

//define binning
double jet_deta[]={-6.2,-4,-3,-2,0,2,3,4,6.2};
double jet_dphi[]{-6.2,-4,-3,-2,-1,0,1,2,3,4,6.2};
double jet_dr[]={0,2,3,4,4.5,5,5.5,6,7,8,10};
double lead_jet_eta[]={-5,-4,-3.5,-3,-2.5,-2,-1,0,1,1.5,2,2.5,3,3.5,4,5};
double lead_jet_phi[]={-3.2,-2,-1,0,1,2,3.2};
double lead_jet_pt[]={0,50,60,70,80,90,100,125,150,175,200,225,250,275,300};
double lep_deta[]={-2.5,-1.5,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.5,2.5};
double lep_dphi[]={-6.2,-4,-2,-1,-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8,1,2,4,6.2};
double lep_dr[]={0,0.2,0.4,0.6,0.8,1,1.5,2,2.5,3.5,4,4.5,5,5.25,5.5,5.75,6,6.5};
double met[]={0,25,50,75,100,125,150,200,300};
double mjj[]={0,250,500,750,1000,1250,1500,1750,2000,2250,2500,3000,4000,5000};
double mll[]={0,15,20,25,30,35,40,45,50,55,60,70,80,100};
