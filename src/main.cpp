#include "loop_ntuple.h"
#include <TTree.h>
#include <vector>
#include <string>
#include <TH1F.h>
#include <TFile.h>
#include <iostream>
#include <TPad.h>
#include <TGraph.h>
#include <fstream>
#include <iostream>
#include "binning_analysis.h"

using namespace std;
void draw(loop_ntuple*);
void ratio(loop_ntuple* no_cut,loop_ntuple *cut);
void plot_ratio_cuts(TTree *truth_VBF,loop_ntuple* no_cut,double cut);

void plot_ratio_cuts(TTree *truth_VBF,loop_ntuple* no_cut,double cut){
  truth_VBF->ResetBranchAddresses();
  std::string name=std::to_string(int(100*cut))+"_cut";
  loop_ntuple *cut_truth=new loop_ntuple(truth_VBF,name.c_str(),40,-5,5);
  cut_truth->fill_hists(cut);
  ratio(no_cut,cut_truth);

}
int main(){
  TFile* tree_file = TFile::Open("../../ntuples/eval_3D_MET_AllSamps_VBF_SR_Feb5.root");
  TTree *VBF_tree = (TTree*)tree_file->Get("VBF");
  //TTree *Zjets_tree=(TTree*)tree_file->Get("Zjets");

  TFile *truth_file=TFile::Open("../../ntuples/merged-output.root");
  TTree* truth_VBF=(TTree*)truth_file->Get("tree");
  // loop_ntuple *vbf=new loop_ntuple(VBF_tree,"vbf",20,0,.1);
  //loop_ntuple *zjets=new loop_ntuple(Zjets_tree,"zjets",20,0,.1);
  
  loop_ntuple *no_cut=new loop_ntuple(truth_VBF,"no_cuts_",40,-5,5);
  //no_cut->vip_hists();
  
  std::cout<<"filling_hists \n";
  no_cut->fill_hists(1);
  determine_binning(no_cut->mjj_truth,0,5000);

  truth_VBF->ResetBranchAddresses();
  std::cout<<"here\n";
  loop_ntuple *cut=new loop_ntuple(truth_VBF,"bdt_cuts_",40,-5,5);
  cut->fill_hists(0.3);
  ratio(no_cut,cut);
  
  //for(int i=0; i< 50;i++){
  //  double cut=(double)i/100.0;
  //  plot_ratio_cuts(truth_VBF,truth,cut);
  // }
  //  draw(truth);
 
  // draw(cut_truth)
  //std::vector<TH1F*> histos={vbf->bdt_Zjets_hist,zjets->bdt_Zjets_hist};
  //DrawHistograms<TH1F>(histos);
  

}

void draw(loop_ntuple *truth){
 
  std::vector<TH1F*> l_jet_phi={truth->lead_jet_phi_truth,truth->lead_jet_phi_reco};
  DrawHistograms<TH1F>(l_jet_phi,"pdf","lead_jet_phi");
  
  std::vector<TH1F*> sub_jet_phi={truth->sub_jet_phi_truth,truth->sub_jet_phi_reco};
  DrawHistograms<TH1F>(sub_jet_phi,"pdf","sub_jet_phi");

  std::vector<TH1F*> l_jet_eta={truth->lead_jet_eta_truth,truth->lead_jet_eta_reco};
  DrawHistograms<TH1F>(l_jet_eta,"pdf","lead_jet_eta");

  std::vector<TH1F*> sub_jet_eta={truth->lead_jet_eta_truth,truth->lead_jet_eta_reco};
  DrawHistograms<TH1F>(sub_jet_eta,"pdf","sub_jet_eta");

  std::vector<TH1F*> jet_dphi={truth->jet_dphi_truth,truth->jet_dphi_reco};
  DrawHistograms<TH1F>(jet_dphi,"pdf","jet_dphi");

  std::vector<TH1F*> jet_dEta={truth->jet_dEta_truth,truth->jet_dEta_reco};
  DrawHistograms<TH1F>(jet_dEta,"pdf","jet_dEta");

  std::vector<TH1F*> jet_dr={truth->jet_dr_truth,truth->jet_dr_reco};
  DrawHistograms<TH1F>(jet_dr,"pdf","jet_dr");

  std::vector<TH1F*> l_jet_pt={truth->lead_jet_pt_truth,truth->lead_jet_pt_reco};
  DrawHistograms<TH1F>(l_jet_pt,"pdf","lead_jet_pt");

  std::vector<TH1F*> sub_jet_pt={truth->sub_jet_pt_truth,truth->sub_jet_pt_reco};
  DrawHistograms<TH1F>(sub_jet_pt,"pdf","sub_jet_pt");

  std::vector<TH1F*> mjj={truth->mjj_truth,truth->mjj_reco};
  DrawHistograms<TH1F>(mjj,"pdf","mjj");

  std::vector<TH1F*> mll={truth->mll_truth,truth->mll_reco};
  std::cout<<"mll ratio\n";
  std::cout<<DrawHistograms<TH1F>(mll,"pdf","mll")<<"\n";


}
void ratio(loop_ntuple * no_cuts, loop_ntuple *cuts){
  ofstream ratio_data_file;
  ratio_data_file.open("ratio_data.txt",ios::app);
  ratio_data_file<<cuts->name;
  ratio_data_file<<",";
  //truth
  std::vector<TH1F*> l_jet_phi={no_cuts->lead_jet_phi_truth,cuts->lead_jet_phi_truth};
  ratio_data_file<<DrawHistograms<TH1F>(l_jet_phi,"pdf","lead_jet_phi_truth");
  ratio_data_file<<",";
  
  std::vector<TH1F*> sub_jet_phi={no_cuts->sub_jet_phi_truth,cuts->sub_jet_phi_truth};
  ratio_data_file<<DrawHistograms<TH1F>(sub_jet_phi,"pdf","sub_jet_phi_truth");
  ratio_data_file<<",";
  
  std::vector<TH1F*> l_jet_eta={no_cuts->lead_jet_eta_truth,cuts->lead_jet_eta_truth};
  ratio_data_file<<DrawHistograms<TH1F>(l_jet_eta,"pdf","lead_jet_eta");
  ratio_data_file<<",";

  std::vector<TH1F*> sub_jet_eta={no_cuts->lead_jet_eta_truth,cuts->lead_jet_eta_truth};
  ratio_data_file<<DrawHistograms<TH1F>(sub_jet_eta,"pdf","sub_jet_eta_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> jet_dphi={no_cuts->jet_dphi_truth,cuts->jet_dphi_truth};
  ratio_data_file<<DrawHistograms<TH1F>(jet_dphi,"pdf","jet_dphi_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> jet_dEta={no_cuts->jet_dEta_truth,cuts->jet_dEta_truth};
  ratio_data_file<<DrawHistograms<TH1F>(jet_dEta,"pdf","jet_dEta_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> jet_dr={no_cuts->jet_dr_truth,cuts->jet_dr_truth};
  ratio_data_file<<DrawHistograms<TH1F>(jet_dr,"pdf","jet_dr_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> lep_dphi={no_cuts->lep_dphi_truth,cuts->lep_dphi_truth};
  ratio_data_file<<DrawHistograms<TH1F>(lep_dphi,"pdf","lep_dphi_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> lep_dEta={no_cuts->lep_dEta_truth,cuts->lep_dEta_truth};
  ratio_data_file<<DrawHistograms<TH1F>(lep_dEta,"pdf","lep_dEta_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> lep_dr={no_cuts->lep_dr_truth,cuts->lep_dr_truth};
  ratio_data_file<<DrawHistograms<TH1F>(lep_dr,"pdf","lep_dr_truth");
  ratio_data_file<<",";
  
  std::vector<TH1F*> l_jet_pt={no_cuts->lead_jet_pt_truth,cuts->lead_jet_pt_truth};
  ratio_data_file<<DrawHistograms<TH1F>(l_jet_pt,"pdf","lead_jet_pt_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> sub_jet_pt={no_cuts->sub_jet_pt_truth,cuts->sub_jet_pt_truth};
  ratio_data_file<<DrawHistograms<TH1F>(sub_jet_pt,"pdf","sub_jet_pt_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> mjj={no_cuts->mjj_truth,cuts->mjj_truth};
  ratio_data_file<<DrawHistograms<TH1F>(mjj,"pdf","mjj_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> mll={no_cuts->mll_truth,cuts->mll_truth};
  ratio_data_file<<DrawHistograms<TH1F>(mll,"pdf","mll_truth");
  ratio_data_file<<",";

  std::vector<TH1F*> t_met={no_cuts->Truth_MET,cuts->Truth_MET};
  ratio_data_file<<DrawHistograms<TH1F>(t_met,"pdf","MET_Truth");
  ratio_data_file<<",";

  //reco
  std::vector<TH1F*> l_jet_phi_reco={no_cuts->lead_jet_phi_reco,cuts->lead_jet_phi_reco};
  ratio_data_file<<DrawHistograms<TH1F>(l_jet_phi_reco,"pdf","lead_jet_phi_reco");
  ratio_data_file<<",";
  
  std::vector<TH1F*> sub_jet_phi_reco={no_cuts->sub_jet_phi_reco,cuts->sub_jet_phi_reco};
  ratio_data_file<<DrawHistograms<TH1F>(sub_jet_phi_reco,"pdf","sub_jet_phi_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> l_jet_eta_reco={no_cuts->lead_jet_eta_reco,cuts->lead_jet_eta_reco};
  ratio_data_file<<DrawHistograms<TH1F>(l_jet_eta_reco,"pdf","lead_jet_eta");
  ratio_data_file<<",";

  std::vector<TH1F*> sub_jet_eta_reco={no_cuts->lead_jet_eta_reco,cuts->lead_jet_eta_reco};
  ratio_data_file<<DrawHistograms<TH1F>(sub_jet_eta_reco,"pdf","sub_jet_eta_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> jet_dphi_reco={no_cuts->jet_dphi_reco,cuts->jet_dphi_reco};
  ratio_data_file<<DrawHistograms<TH1F>(jet_dphi_reco,"pdf","jet_dphi_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> jet_dEta_reco={no_cuts->jet_dEta_reco,cuts->jet_dEta_reco};
  ratio_data_file<<DrawHistograms<TH1F>(jet_dEta_reco,"pdf","jet_dEta_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> jet_dr_reco={no_cuts->jet_dr_reco,cuts->jet_dr_reco};
  ratio_data_file<<DrawHistograms<TH1F>(jet_dr_reco,"pdf","jet_dr_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> l_jet_pt_reco={no_cuts->lead_jet_pt_reco,cuts->lead_jet_pt_reco};
  ratio_data_file<<DrawHistograms<TH1F>(l_jet_pt_reco,"pdf","lead_jet_pt_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> sub_jet_pt_reco={no_cuts->sub_jet_pt_reco,cuts->sub_jet_pt_reco};
  ratio_data_file<<DrawHistograms<TH1F>(sub_jet_pt_reco,"pdf","sub_jet_pt_reco");
  ratio_data_file<<",";

  std::vector<TH1F*> mjj_reco={no_cuts->mjj_reco,cuts->mjj_reco};
  ratio_data_file<<DrawHistograms<TH1F>(mjj_reco,"pdf","mjj_reco");
  ratio_data_file<<"\n";


  ratio_data_file.close();
}
void analyze_cuts(loop_ntuple * vbf, loop_ntuple *zjets){
  std::vector<double> cuts;
  std::vector<double> vbf_eff;
  std::vector<double> zjets_eff;
  double cut;
  double norm;
  for(int i=0;i<1000;i++){
    cut=(double)i/1000.0;
    cuts.push_back(cut);
    vbf_eff.push_back(vbf->effeciency(cut));
    zjets_eff.push_back(zjets->effeciency(cut));
    if(i==999) norm=vbf_eff[i];
  }
  for(int j=0;j<1000;j++){
    vbf_eff[j]= vbf_eff[j]/norm;
    zjets_eff[j]= zjets_eff[j]/ norm;
  }
  for(int k=0;k<1000;k++){
    if(k%10==0)
      std::cout<<(double)k/1000.0<<" "<<vbf_eff[k]<<" "<<zjets_eff[k]<<"\n";
  }
  
  TGraph * eff_gr= new TGraph(100,&cuts[0],&vbf_eff[0]);
  eff_gr->SetTitle("vbf effeciency");
  eff_gr->GetXaxis()->SetTitle("bdt cut");
  DrawHist<TGraph>(eff_gr);
 
  TH1F * ratio=(TH1F*)vbf->bdt_Zjets_hist->Clone();
  ratio->Divide(zjets->bdt_Zjets_hist);
  ratio->SetTitle("Vbf/Zjets ratio ");
  DrawHist<TH1F>(ratio);
  

}
