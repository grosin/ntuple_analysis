#include <TNtuple.h>
#include <vector>
#include <TH1F.h>
#include <TH1D.h>
#include <TH2D.h>
#include <string>
#include "loop_ntuple.h"
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include "binning_analysis.h"
using namespace TMath;
loop_ntuple::loop_ntuple(TTree* tnt,std::string name,int nbins,double min,double max){
  my_tree=tnt;
  this->name=name;
  
  mjj_truth=new TH1F((name+"truth_mjj").c_str(),"mjj truth",nbins,0,5000);
  mjj_reco=new TH1F((name+"reco_mjj").c_str(),"mjj  reco",nbins,0,5000);
  
  mll_truth=new TH1F((name+"truth_mll").c_str(),"mll truth",nbins,0,100);
  mll_reco=new TH1F((name+"reco_mll").c_str(),"mll reco",nbins,0,100);
  
  bdt_Zjets_hist=new TH1F(name.c_str(),name.c_str(),nbins,min,max);
  lead_jet_phi_truth=new TH1F((name+"truth_lead_jet_phi").c_str(),"truth lead_jet_phi",nbins,min,max);
  sub_jet_phi_truth=new TH1F((name+"truth_sub_jet_phi").c_str(),"truth sub_jet_phi",nbins,min,max);
  lead_jet_eta_truth=new TH1F((name+"truth_lead_jet_eta").c_str(),"truth lead_jet_eta",nbins,min,max);
  sub_jet_eta_truth=new TH1F((name+"truth_sub_jet_eta").c_str(),"truth sub_jet_eta",nbins,min,max);

  lead_jet_pt_truth=new TH1F((name+"truth_lead_jet_pt").c_str(),"truth lead_jet_pt",nbins,30,300);
  sub_jet_pt_truth=new TH1F((name+"truth_sub_jet_pt").c_str(),"truth sub_jet_pt",nbins,30,300);

  //scalar
  jet_dphi_truth=new TH1F((name+"truth_jet_dphi").c_str(),"truth jet_dphi",nbins,min,max);
  jet_dr_truth=new TH1F((name+"truth_jet_dr").c_str(),"truth jet_dr",nbins,0,10);
  jet_dEta_truth=new TH1F((name+"truth_jet_deta").c_str(),"truth jet_deta",nbins,min,max);

  lep_dphi_truth=new TH1F((name+"truth_lep_dphi").c_str(),"truth lep_dphi",nbins,min,max);
  lep_dr_truth=new TH1F((name+"truth_lep_dr").c_str(),"truth lep_dr",nbins,0,10);
  lep_dEta_truth=new TH1F((name+"truth_lep_deta").c_str(),"truth lep_deta",nbins,-2.5,2.5);


  //reco hists    
  lead_jet_phi_reco=new TH1F((name+"reco_lead_jet_phi").c_str(),"reco lead_jet_phi",nbins,min,max);
  sub_jet_phi_reco=new TH1F((name+"reco_sub_jet_phi").c_str(),"reco sub_jet_phi",nbins,min,max);
  lead_jet_eta_reco=new TH1F((name+"reco_lead_jet_eta").c_str(),"reco lead_jet_eta",nbins,min,max);
  sub_jet_eta_reco=new TH1F((name+"reco_sub_jet_eta").c_str(),"reco sub_jet_eta",nbins,min,max);

  lead_jet_pt_reco=new TH1F((name+"reco_lead_jet_pt").c_str(),"reco lead_jet_pt",nbins,30,300);
  sub_jet_pt_reco=new TH1F((name+"reco_sub_jet_pt").c_str(),"reco sub_jet_pt",nbins,30,300);

  //scalar
  jet_dphi_reco=new TH1F((name+"reco_jet_dphi").c_str(),"reco jet_dphi",nbins,min,max);
  jet_dr_reco=new TH1F((name+"reco_jet_dr").c_str(),"reco jet_dr",nbins,0,10);
  jet_dEta_reco=new TH1F((name+"reco_jet_deta").c_str()," reco jet_deta",nbins,min,max);
  detall_reco=new TH1D((name+"detall_reco").c_str(),"detall reco",nbins,-2.5,2.5);
  detall_true=new TH1D((name+"detall_true").c_str(),"detall truth",nbins,-2.5,2.5);

  
  detajj_trueAreco_rw_reco=new TH1D((name+"deta_fiducial_num").c_str(),"fiducial num",nbins,min,max);
  detajj_reco=new TH1D((name+"detajj_reco").c_str(),"detajj reco",nbins,min,max);
  detajj_trueAreco_rw_true=new TH1D((name+"deta_eff_num").c_str(),"effeciency num",nbins,min,max);
  detajj_true=new TH1D((name+"true_jet_deta").c_str(),"detajj truth",nbins,min,max);
  detajj_trueAreco_tw_true=new TH1D((name+"deta_purity_denom").c_str(),"purity denom",nbins,min,max);
  detajj_recoSel_rw_true=new TH1D((name+"deta_stability").c_str(),"stability",nbins,min,max);
  detajj_migration=new TH2D((name+"deta_migration").c_str(),"deta migration matrix",nbins,min,max,nbins,min,max);

  detall_recoSel_rw_true=new TH1D((name+"detall_stability").c_str(),"detall_stability",nbins,-2.5,2.5);
  detall_migration=new TH2D((name+"detall_migration").c_str(),"detall_migration",nbins,-2.5,2.5,nbins,-2.5,2.5);
  detall_trueAreco_rw_reco=new TH1D((name+"detall_fiducial_num").c_str(),"detall fiducial num",nbins,-2.5,2.5);
  detall_trueAreco_rw_true=new TH1D((name+"detall_eff_num").c_str(),"detall_eff_num",nbins,-2.5,2.5);
  detall_trueAreco_tw_true=new TH1D((name+"detall_purity_denom").c_str(),"detall_purity_denom",nbins,-2.5,2.5);

  mjj_recoSel_rw_true=new TH1D((name+"mjj_stability").c_str(),"mjj stability",nbins,0,5000);
  mjj_migration=new TH2D((name+"mjj_migration").c_str(),"mjj migration",nbins,0,5000,nbins,0,5000);
  mjj_trueAreco_rw_reco=new TH1D((name+"mjj_fiducial_num").c_str(),"mjj fiducial num",nbins,0,5000);
  mjj_trueAreco_rw_true=new TH1D((name+"mjj_eff_num").c_str(),"mjj effciency num",nbins,0,5000);
  mjj_trueAreco_tw_true=new TH1D((name+"mjj_purity_denom").c_str(),"mjj purity denom",nbins,0,5000);

  mll_recoSel_rw_true=new TH1D((name+"mll_stability").c_str(),"mll stability",nbins,0,100);
  mll_migration=new TH2D((name+"mll_migration").c_str(),"mll_migration",nbins,0,100,nbins,0,100);
  mll_trueAreco_rw_reco=new TH1D((name+"mll_fiducial_num").c_str(),"mll_fiducial_num",nbins,0,100);
  mll_trueAreco_rw_true=new TH1D((name+"mll_eff_num").c_str(),"mll effeciency num",nbins,0,100);
  mll_trueAreco_tw_true=new TH1D((name+"mll_purity_denom").c_str(),"mll purity denom",nbins,0,100);
  
  Truth_MET=new TH1F((name+"Truth_Met").c_str(),"Truth Met",nbins,30,300);
  std::cout<<"here \n";
  my_tree->SetBranchAddress("weight", &weight);
  my_tree->SetBranchAddress("r_weight", &r_weight);

  my_tree->SetBranchAddress("bdt_met",&bdt_met);
  
  truth_jet_phi=new std::vector<float>();
  truth_jet_eta=new std::vector<float>();
  truth_jet_pt=new std::vector<float>();
  
  my_tree->SetBranchAddress("jet_phi", &truth_jet_phi);
  my_tree->SetBranchAddress("jet_eta", &truth_jet_eta);
  my_tree->SetBranchAddress("jet_pt", &truth_jet_pt);


  reco_jet_phi=new std::vector<float>();
  reco_jet_eta=new std::vector<float>();
  reco_jet_pt=new std::vector<float>();


  truth_lep_phi=new std::vector<float>();
  truth_lep_eta=new std::vector<float>();
  truth_lep_pt=new std::vector<float>();
  
  my_tree->SetBranchAddress("lep_phi", &truth_lep_phi);
  my_tree->SetBranchAddress("lep_eta", &truth_lep_eta);
  my_tree->SetBranchAddress("lep_pt", &truth_lep_pt);


  reco_lep_phi=new std::vector<float>();
  reco_lep_eta=new std::vector<float>();
  reco_lep_pt=new std::vector<float>();

  
  my_tree->SetBranchAddress("r_lep_phi", &reco_lep_phi);
  my_tree->SetBranchAddress("r_lep_eta", &reco_lep_eta);
  my_tree->SetBranchAddress("r_lep_pt", &reco_lep_pt);
  
  
  my_tree->SetBranchAddress("r_jet_phi", &reco_jet_phi);
  my_tree->SetBranchAddress("r_jet_eta", &reco_jet_eta);
  my_tree->SetBranchAddress("r_jet_pt", &reco_jet_pt);
  
  my_tree->SetBranchAddress("r_passedRecoSave",&passReco);
  my_tree->SetBranchAddress("Truth_Met",&MET);
  my_tree->SetBranchAddress("r_passedFiducialSave",&fiducial);
  my_tree->SetBranchAddress("r_passedPreselection",&preSelection);
  my_tree->SetBranchAddress("r_passedCJV",&passedCJV);
  my_tree->SetBranchAddress("r_mjj",&r_mjj);
  my_tree->SetBranchAddress("r_mll",&r_mll);
  my_tree->SetBranchAddress("mjj",&mjj);
  my_tree->SetBranchAddress("mll",&mll);
  
}

void loop_ntuple::fill_met_hists(){
  //produce effeciency plot
  for(int i=0;i<my_tree->GetEntries();i++ ){
    my_tree->GetEntry(i);
    bdt_Zjets_hist->Fill(bdt_met,weight);
  } 
}//end of fill hist
void loop_ntuple::vip_hists(){
  double reco_detajj=-100;
  double true_detajj=-100;
  double reco_detall=-100;
  double true_detall=-100;
  bool truth_cut;
  bool reco_cut;
  for (Int_t j=0;j<my_tree->GetEntries();j++){
    my_tree->GetEvent(j);      
    truth_cut=fiducial==1;
    reco_cut=passReco==1;
    try{
      true_detajj=truth_jet_eta->at(0)-truth_jet_eta->at(1);
      true_detall=truth_lep_eta->at(0)-truth_lep_eta->at(1);
      
      if(truth_cut){
        detajj_true->Fill(true_detajj);
        detall_true->Fill(true_detall);
        mll_truth->Fill(mll);
        mjj_truth->Fill(mjj);
        std::cout<<weight<<"\n";
      }
    }catch(std::out_of_range &e){
          std::cout<<"out of  range issues in truth\n";
    }
    try{
      reco_detajj=reco_jet_eta->at(0)-reco_jet_eta->at(1);
      reco_detall=reco_lep_eta->at(0)-reco_lep_eta->at(1);
    if(reco_cut){
      detajj_reco->Fill(reco_detajj,r_weight);
      detall_reco->Fill(reco_detall,r_weight);
      mjj_reco->Fill(r_mjj/1000,r_weight);
      mll_reco->Fill(r_mll/1000,r_weight);

      if(true_detajj>-99)
        detajj_recoSel_rw_true->Fill(true_detajj,r_weight);
      if(true_detall>-99)
        detall_recoSel_rw_true->Fill(true_detall,r_weight);
      if(mjj>0)
        mjj_recoSel_rw_true->Fill(mjj,r_weight);
      if(mll>0)
        mll_recoSel_rw_true->Fill(mll,r_weight);

      std::cout<<r_weight<<"\n";

    } 
    }catch(std::out_of_range &e){
          std::cout<<"out of  range issues in reco\n";
    }
    if(reco_cut and truth_cut){
      detajj_trueAreco_rw_reco->Fill(reco_detajj,r_weight);
      detajj_trueAreco_rw_true->Fill(true_detajj,r_weight);
      detajj_trueAreco_tw_true->Fill(true_detajj);
      detajj_migration->Fill(reco_detajj,true_detajj,r_weight);
        
      detall_trueAreco_rw_reco->Fill(reco_detall,r_weight);
      detall_trueAreco_rw_true->Fill(true_detall,r_weight);
      detall_trueAreco_tw_true->Fill(true_detall);
      detall_migration->Fill(reco_detall,true_detall,r_weight);

      mjj_trueAreco_rw_reco->Fill(r_mjj/1000,r_weight);
      mjj_trueAreco_rw_true->Fill(mjj,r_weight);
      mjj_trueAreco_tw_true->Fill(mjj);
      mjj_migration->Fill(r_mjj/1000,mjj,r_weight);

      mll_trueAreco_rw_reco->Fill(r_mll/1000,r_weight);
      mll_trueAreco_rw_true->Fill(mll,r_weight);
      mll_trueAreco_tw_true->Fill(mll);
      mll_migration->Fill(r_mll/1000,mll,r_weight);
          
    }
  }

  TFile f1("vip_hists.root","RECREATE");
  detajj_reco->Write();
  detajj_true->Write();
  detajj_recoSel_rw_true->Write();
  detajj_trueAreco_rw_reco->Write();
  detajj_trueAreco_rw_true->Write();
  detajj_trueAreco_tw_true->Write();
  detajj_migration->Write();
  
  detall_reco->Write();
  detall_true->Write();
  detall_recoSel_rw_true->Write();
  detall_trueAreco_rw_reco->Write();
  detall_trueAreco_rw_true->Write();
  detall_trueAreco_tw_true->Write();
  detall_migration->Write();

  mjj_reco->Write();
  mjj_truth->Write();
  mjj_recoSel_rw_true->Write();
  mjj_trueAreco_rw_reco->Write();
  mjj_trueAreco_rw_true->Write();
  mjj_trueAreco_tw_true->Write();
  mjj_migration->Write();

  mll_reco->Write();
  mll_truth->Write();
  mll_recoSel_rw_true->Write();
  mll_trueAreco_rw_reco->Write();
  mll_trueAreco_rw_true->Write();
  mll_trueAreco_tw_true->Write();
  mll_migration->Write();
}
void loop_ntuple::fill_hists(double cut){
  double dphi;
  double deta;
  double dr;

  double true_detall;
  double true_drll;
  double true_dphill;
  double reco_dphi;
  double reco_deta;
  double reco_dr;
  
  bool truth_cut;
  for (Int_t j=0;j<my_tree->GetEntries();j++){
    my_tree->GetEvent(j);      
    try{
      truth_cut=(fiducial==1);// && preSelection==1 && passedCJV==1);
      if(passReco==1 && bdt_met < cut){
          reco_dphi=reco_jet_phi->at(0)-reco_jet_phi->at(1);
          reco_deta=reco_jet_eta->at(0)-reco_jet_eta->at(1);
 
          reco_dr=Power(Power(deta,2)+Power(dphi,2),0.5);
          lead_jet_phi_reco->Fill(reco_jet_phi->at(0),weight);
          sub_jet_phi_reco->Fill(reco_jet_phi->at(1),weight);
          lead_jet_eta_reco->Fill(reco_jet_eta->at(0),weight);
          sub_jet_eta_reco->Fill(reco_jet_eta->at(1),weight);
          
          lead_jet_pt_reco->Fill(reco_jet_pt->at(0)/1000,weight);
          sub_jet_pt_reco->Fill(reco_jet_pt->at(1)/1000,weight);
          jet_dphi_reco->Fill(reco_dphi,weight);
          jet_dEta_reco->Fill(reco_deta,weight);
          jet_dr_reco->Fill(reco_dr,weight);
          mjj_reco->Fill(r_mjj/1000,weight);
          mll_reco->Fill(r_mll/1000);

      }
      if( passReco && truth_cut && bdt_met < cut){
        try{
          Truth_MET->Fill(MET);
          dphi=truth_jet_phi->at(0)-truth_jet_phi->at(1);
          deta=truth_jet_eta->at(0)-truth_jet_eta->at(1);
          true_detall=truth_lep_eta->at(0)-truth_lep_eta->at(1);
          true_dphill=truth_lep_phi->at(0)-truth_lep_phi->at(1);
          true_drll=Power(Power(true_detall,2)+Power(true_dphill,2),0.5);

          lep_dphi_truth->Fill(true_dphill);
          lep_dEta_truth->Fill(true_detall);
          lep_dr_truth->Fill(true_drll);
          
          dr=Power(Power(deta,2)+Power(dphi,2),0.5);
          lead_jet_phi_truth->Fill(truth_jet_phi->at(0),weight);

          sub_jet_phi_truth->Fill(truth_jet_phi->at(1),weight);
          lead_jet_eta_truth->Fill(truth_jet_eta->at(0) ,weight);
          sub_jet_eta_truth->Fill(truth_jet_eta->at(1),weight);
          lead_jet_pt_truth->Fill(truth_jet_pt->at(0),weight);
          sub_jet_pt_truth->Fill(truth_jet_pt->at(1),weight);
          
          jet_dphi_truth->Fill(dphi,weight);
          jet_dEta_truth->Fill(deta,weight);
          jet_dr_truth->Fill(dr,weight);
          mjj_truth->Fill(mjj,weight);
          mll_truth->Fill(mll,weight);
        }catch(std::out_of_range &e){
          std::cout<<"out of  range issues in truth\n";
        }
      }
    }catch(std::out_of_range &e){
          std::cout<<"out of range issues in general\n";
    }
  }


}

double loop_ntuple::effeciency(double cut){
  double events=0;
  bool truth_cut;
  for(int i=0;i<my_tree->GetEntries();i++ ){
    my_tree->GetEntry(i);
    //truth_cut=(fiducial==1 && preSelection==1, passReco==1 );
    if(bdt_met < cut )
      events+=weight;
  }
  return events;
}



