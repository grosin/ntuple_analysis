#pragma once
#include <vector>
#include <TH1F.h>
void print_vector(std::vector<double> vec);
void determine_binning(TH1F * hist,double min,double max);
