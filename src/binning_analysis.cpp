#include <iostream>
#include <TH1F.h>
#include <vector>
#include <TMath.h>
using namespace TMath;

void print_vector(std::vector<double> vec){
  std::cout<<"[";
  for(auto &i : vec){
    std::cout<<i<<" ";
  }
  std::cout<<"]\n";
}
void determine_binning(TH1F * hist,double min, double max){
  int nbins=hist->GetSize();
  // double min=hist->GetMinimumBin();
  //double max=hist->GetMaximumBin();
  std::cout<<"number of  bins "<<nbins<<"\n";
  std::cout<<"max "<<max<<"\n";
  std::cout<<"min "<<min<<"\n";

  double bin_size=(max-min)/(double)nbins;
  std::vector<double> binning={min};

  double bin_content=0;
  double bin_error=0;
  int j=0;
  std::cout<<"bin size "<<bin_size<<"\n";
  for(int i=0;i<nbins;i++){
    j=0;
    bin_content=hist->GetBinContent(i);
    bin_error=hist->GetBinError(i);
    while(true){
      j++;
      if(i+j >nbins)
        {
          binning.push_back(bin_size*i+min);
          break;
        }
      bin_content=bin_content+hist->GetBinContent(i+j);
      bin_error=Power(Power(bin_error,2)+Power(hist->GetBinError(i+j),2),0.5);
      if(bin_error/bin_content > 0.1){
        binning.push_back(bin_size*i+min);
        i=i+j;
        break;
      }
    } //end of while
  }//end of for loop
  print_vector(binning);
}

