#include <vector>
#include <TMath.h>
using namespace TMath;
bool GevCorrection;

vector<double> GetMinMax(TTree * VBF,const char * distribution);
void plot_correlation();

void analyze(const char * dist){
  TFile* TruthVBFFile = TFile::Open("../ntuples/merged-output.root");
  TNtuple *truth_VBF = (TNtuple*)TruthVBFFile->Get("tree");
  const char * truth_distribution=dist;
  char result[1024];
  result[0]='\0';
  strcat(result, "r_");
  strcat(result,dist);
  const char * reco_distribution=result;
  plot_correlation();
}

void plot_correlation(){
  TFile* TruthVBFFile = TFile::Open("../ntuples/merged-output.root");
  TNtuple *data = (TNtuple*)TruthVBFFile->Get("tree");
  double met1;
  float met2;
  float weight;
  int passReco;
  int fiducial;
  int preSelection;
  int passedCJV;
  int passedOLV;
  
  vector<double> range1= GetMinMax(data,"met_vector");
  vector<double> range2=GetMinMax(data,"bdt_Zjets");

  range1[1]=200;
  range2[1]=0.1;
  int nbins=20;
  TH2F * correlation=new TH2F("met_correlation","MET_correlation",nbins,range1[0],range1[1],nbins,range2[0],range2[1]);
  data->SetBranchAddress("weight", &weight);
  data->SetBranchAddress("r_passedRecoSave",&passReco);
  data->SetBranchAddress("r_passedFiducialSave",&fiducial);
  data->SetBranchAddress("r_passedPreselection",&preSelection);
  data->SetBranchAddress("r_passedCJV",&passedCJV);
  data->SetBranchAddress("r_passedOLV",&passedOLV);
  data->SetBranchAddress("met_vector",&met1);
  data->SetBranchAddress("bdt_Zjets",&met2);

  for(int i =0 ; i< data->GetEntries();i++){
    data->GetEntry(i);
    if(passReco==1 &&fiducial==1 && preSelection==1 )
      correlation->Fill(met1,met2,weight);
  }
  TCanvas *c1 = new TCanvas("c1", "c1",1000,1000);
  correlation->GetXaxis()->SetTitle("truth MET");
  correlation->GetYaxis()->SetTitle("BDT MET");
  correlation->Draw("colz");

}
void plot_jet(TTree *truth_VBF, const char * truth_distribution,const char * detector_distribution){
  std::vector<double> minMax=GetMinMax(truth_VBF,truth_distribution);

  double min_bin=minMax[0];
  double max_bin=minMax[1];
  TH1F *truth_hist= new TH1F("truth_hist",truth_distribution,20,min_bin,max_bin);
  TH1F *reco_hist= new TH1F("reco_hist",detector_distribution,20,min_bin,max_bin);

  TH2F * lead_sublead_correlation=new TH2F("correlation","jet correlation",20,min_bin,max_bin,20,min_bin,max_bin);
  
  std::cout<<"bin_range:"<<min_bin<<" "<<max_bin<<"\n";

  TH2F * jet_phi_corr= new TH2F("jet_phi_corr","jet phi correlation",50,min_bin,max_bin,50,min_bin,max_bin);
  std::vector<float>* truth_lep_phi=new std::vector<float>();
  std::vector<float> *detector_lep_phi= new std::vector<float>();

  std::vector<float>* truth_lep_eta=new std::vector<float>();
  std::vector<float> *detector_lep_eta= new std::vector<float>();
    
  float weight;
  int passReco;
  int fiducial;
  int preSelection;
  int passedCJV;
  int passedOLV;
  
  //truth_VBF->SetBranchAddress(truth_distribution, &truth_data);
  //truth_VBF->SetBranchAddress(detector_distribution, &detector_data);
  truth_VBF->SetBranchAddress("jet_phi", &truth_lep_phi);
  truth_VBF->SetBranchAddress("r_jet_phi", &detector_lep_phi);

  truth_VBF->SetBranchAddress("jet_eta", &truth_lep_eta);
  truth_VBF->SetBranchAddress("r_jet_eta", &detector_lep_eta);

  truth_VBF->SetBranchAddress("weight", &weight);
  truth_VBF->SetBranchAddress("r_passedRecoSave",&passReco);
  truth_VBF->SetBranchAddress("r_passedFiducialSave",&fiducial);
  truth_VBF->SetBranchAddress("r_passedPreselection",&preSelection);
  truth_VBF->SetBranchAddress("r_passedCJV",&passedCJV);
  truth_VBF->SetBranchAddress("r_passedOLV",&passedOLV);
  
  Int_t truth_nevents = truth_VBF->GetEntries();
  
 
  bool truth_cut;
  bool reco_cut;
  std::cout<<"bin_range:"<<min_bin<<" "<<max_bin<<"\n";
  float corr;
  int index;
  GevCorrection ? corr=1000 : corr=1;
  double dphi;
  double r_dphi;
  double deta;
  double r_deta;
  double dr;
  double r_dr;
  std::cout<<"starting loop \n";
  std::cout<<truth_nevents<<"\n";
  for (Int_t j=0;j<truth_nevents;j++){
    try{
      truth_VBF->GetEvent(j);
      truth_cut=(fiducial==1 && preSelection==1 && passedCJV==1 && passedOLV==1);
      reco_cut= passReco==1 ;
      //std::cout<<truth_lep_eta->size()<<" "<<detector_lep_eta->size()<<"\n";
      if(truth_cut && reco_cut){
        try{
          dphi=truth_lep_phi->at(0)-truth_lep_phi->at(1);
          r_dphi=detector_lep_phi->at(0) - detector_lep_phi->at(1);
          
          deta=truth_lep_eta->at(0)-truth_lep_eta->at(1);
          r_deta=detector_lep_eta->at(0) - detector_lep_eta->at(1);

          dr=Power(Power(deta,2)+Power(dphi,2),0.5);
          r_dr=Power(Power(r_deta,2)+Power(r_dphi,2),0.5);

          truth_hist->Fill(dr,weight);
          reco_hist->Fill(r_dr/corr,weight);
        }catch(std::out_of_range &e){
          std::cout<<"out of range issues in reco\n";
        }
      
      }
      else if(truth_cut){
        try{
          dphi=truth_lep_phi->at(0)-truth_lep_phi->at(1);
          deta=truth_lep_eta->at(0)-truth_lep_eta->at(1);
          dr=Power(Power(deta,2)+Power(dphi,2),0.5);
          truth_hist->Fill(dr,weight);
        }catch(std::out_of_range &e){
          std::cout<<"out of range issues in truth\n";
        }
      }
    }catch(std::out_of_range &e){
          std::cout<<"out of range issues in general\n";
    }
  }
}

vector<double> GetMinMax(TTree * VBF,const char * distribution){
  gROOT->SetBatch(true);
  long nevent = VBF->GetEntries();
  VBF->SetEstimate(nevent);
  VBF->Draw(distribution,"r_passedFiducialSave==1 && r_passedRecoSave==1");//,distribution_cut);
  double * tree_data=VBF->GetV1();
  std::cout<<"calculating MinMax: "<<nevent<<"\n";
  double* data_min_bin=std::min_element(tree_data, tree_data+nevent);
  double* data_max_bin=std::max_element(tree_data,tree_data+nevent);
  std::cout<<"min :"<<*data_min_bin<<"max: "<<*data_max_bin<<"\n";
  gROOT->SetBatch(false);
  std::vector<double> MinMax(2);
  MinMax[0]=*data_min_bin;
  MinMax[1]=*data_max_bin;
  if(*data_max_bin >100)
    GevCorrection=true;
  else
    GevCorrection=false;
                       
  return MinMax; 
}


